﻿using System.Web.Mvc;

namespace TesseractServer.Controllers.Api
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }
    }
}
