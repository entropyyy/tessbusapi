﻿using DetectionEngine;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using DetectionEngine.Engine;
using DetectionEngine.Interfaces;
using Tesseract;
using TesseractServer.Models;
using TesseractServer.Persistance;
using ImageFormat = System.Drawing.Imaging.ImageFormat;

namespace TesseractServer.Controllers.Api
{
    [RoutePrefix("api/upload")]
    public class UploadController : ApiController
    {
        private readonly IBusLineRepository _busRepository;
        private readonly IImageProcessor _defaultImageProcessor;
        private readonly IImageProcessor _defaultBoxProcessor;
        private readonly ImageLoader _imageLoader;
        private List<SearchResult> results;


        public UploadController()
        {
            results = new List<SearchResult>();
            _defaultImageProcessor = new DefaultImageProcessor();
            _defaultBoxProcessor = new DefaultBoxProcessor();
            _imageLoader = new ImageLoader();
            _busRepository = new BusLineRepository();
        }

        [Route("tesseract")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> PostUploadImage()
        {
            try
            {
                var buses = new List<BusLine>();
                var httpRequest = HttpContext.Current.Request;

                foreach (string file in httpRequest.Files)
                {
                    var postedFile = httpRequest.Files[file];
                    if (postedFile == null || postedFile.ContentLength <= 0) continue;

                    string relativePath = "~/Userimage/" + Path.GetFileName(postedFile.FileName);
                    string physicalPath = HttpContext.Current.Server.MapPath(relativePath);
                    postedFile.SaveAs(physicalPath);

                    var img = _imageLoader.LoadImage(physicalPath);
                    var bus = BoxDetectionEngineFactory.Create();
                    var boxes = bus.GetMatchingBoxes();
                    
                    using (var engine = new TesseractEngine(HttpContext.Current.Server.MapPath(@"~/tessdata"), "eng", EngineMode.Default))
                        foreach (Bitmap bitmap in boxes)
                            using (var image = new System.Drawing.Bitmap(bitmap))
                            using (var pix = PixConverter.ToPix(image))
                            using (var page = engine.Process(pix))
                            {
                                var result = new SearchResult(page.GetText(), page.GetMeanConfidence());
                                if (result.Success)
                                    results.Add(result);

                                buses.Add(_busRepository.GetBus(result.Text));
                            }
                }

                return Ok(buses);
            }
            catch (Exception ex)
            {
                return InternalServerError();
            }
        }
    }


    public class SearchResult
    {
        public string Text { get; set; }
        public double Mean { get; set; }
        public bool Success { get; set; }
        private Match _result;

        public SearchResult(string text, double mean)
        {
            this._result = Regex.Match(text, @"\d{3}");
            this.Success = _result.Success;
            this.Mean = mean;

            if (this.Success)
                this.Text = _result.Value;
        }

    }

}

