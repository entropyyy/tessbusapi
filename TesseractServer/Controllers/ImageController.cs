﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Tesseract;
using TesseractServer.Persistance;
using TesseractServer.ViewModels;
using TesseractServer.ViewModels.Builders;

namespace TesseractServer.Controllers
{
    public class ImageController : Controller
    {
        private readonly IBusLineRepository _busRepository;
        private readonly TesseractResultBusLineViewModelBuilder viewModelBuilder;

        public ImageController()
        {
            viewModelBuilder = new TesseractResultBusLineViewModelBuilder();
            _busRepository = new BusLineRepository();
        }

        // GET: Upload
        public ActionResult FileUpload()
        {
            return View("Upload");
        }

        [HttpPost]
        public ActionResult ProcessImage(UploadImageViewModel fileViewModel)
        {
            var postedFile = fileViewModel.File;

            if (postedFile == null || postedFile.ContentLength <= 0) return View("Upload");

            using (var engine = new TesseractEngine(HttpContext.Server.MapPath(@"~/tessdata"), "eng", EngineMode.Default))
            using (var image = new System.Drawing.Bitmap(postedFile.InputStream))
            using (var pix = PixConverter.ToPix(image))
            using (var page = engine.Process(pix))
            {
                var resultText = page.GetText();

                var sygnature = Regex.Match(resultText, @"\d{3}");

                var bus = _busRepository.GetBus(sygnature.Value);

                var viewModel = this.viewModelBuilder.Build(bus, page, resultText);
                
                string relativePath = "~/Userimage/" + Path.GetFileName(postedFile.FileName);
                string physicalPath = Server.MapPath(relativePath);
                postedFile.SaveAs(physicalPath);
                viewModel.RelativePath = relativePath;

                return View("ProcessImage", viewModel);
            }
        }


        public ActionResult _CreateImageView()
        {            
            return View("_ImageView");
        }
    }
}