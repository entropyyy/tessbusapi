﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using TesseractServer.Models;

namespace TesseractServer.Persistance
{
    public interface IBusLineRepository
    {
        IList<BusLine> GetAllBusses();
        BusLine GetBus(string syganture);
        void AddBus(BusLine bus);
        void DeleteBus(string sygnature);
    }
}