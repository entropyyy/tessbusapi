﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TesseractServer.Models
{
    public class BusLine
    {
        public IList<BusStop> Stops { get; }
        public BusStop End { get; private set; }
        public BusStop Start { get; private set; }
        public string Syganture { get; private set; }

        public BusLine(string syganture)
        {
            Syganture = syganture ?? throw new ArgumentNullException(nameof(syganture));
            Stops = new List<BusStop>();
        }

        public BusLine(string syganture, IEnumerable<BusStop> stops)
        {
            if (stops == null) throw new ArgumentNullException(nameof(stops));
            
            Syganture = syganture ?? throw new ArgumentNullException(nameof(syganture));
            Stops = new List<BusStop>(stops);
        }

        public BusLine AddBusStop(BusStop stop)
        {
            if (stop == null) throw new ArgumentNullException(nameof(stop));
            
            if (!Stops.Any())
                Start = stop;
            
            Stops.Add(stop);

            return this;
        }

        public BusLine SetStart(BusStop stop)
        {
            if (stop == null) throw new ArgumentNullException(nameof(stop));

            Stops.Remove(stop);
            Stops.Insert(0, stop);              
            this.Start = stop;
            return this;
        }

        public BusLine SetEnd(BusStop stop)
        {
            if (stop == null) throw new ArgumentNullException(nameof(stop));
            if (Stops.Last().Name != stop.Name)
            {
                Stops.Remove(stop);
                Stops.Add(stop);
            }

            this.End = stop;
            return this;
        }     
    }
}