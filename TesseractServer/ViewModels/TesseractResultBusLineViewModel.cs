﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace TesseractServer.ViewModels
{
    public class TesseractResultBusLineViewModel
    {
        [DisplayName("Wynik odczytany z prawdopodobieństwem: ")]
        public string MeanConfidence { get; set; }

        [DisplayName("Odczyt: ")]
        public string Result { get; set; }      
        
        [DisplayName("Autobus lini: ")]
        public string BusLineSygnature { get; set; }
        
        [DisplayName("Przystanki: ")]
        public IEnumerable<string> Stops { get; set; }
        
        public string RelativePath { get; set; }
    }
}