﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TesseractServer.ViewModels
{
    public class UploadImageViewModel
    { 
        public HttpPostedFileBase File { get; set; }
    }
}