﻿using System.Collections.Generic;
using System.Linq;
using Tesseract;
using TesseractServer.Models;

namespace TesseractServer.ViewModels.Builders
{
    public class TesseractResultBusLineViewModelBuilder
    {
        public TesseractResultBusLineViewModel Build(BusLine bus, Page page,
            string resultText)
        {
            TesseractResultBusLineViewModel viewModel;
            if (bus == null)
            {
                viewModel = new TesseractResultBusLineViewModel()
                {
                    MeanConfidence = $"{page.GetMeanConfidence():P}",
                    Result = resultText,
                    BusLineSygnature = "Nie odnaleziono",
                    Stops = new List<string>()
                };
            }
            else
            {
                viewModel = new TesseractResultBusLineViewModel()
                {
                    MeanConfidence = $"{page.GetMeanConfidence():P}",
                    Result = resultText,
                    BusLineSygnature = bus.Syganture,
                    Stops = bus.Stops.Select(stop => stop.Name)
                };
            }
            return viewModel;
        }
    }
}